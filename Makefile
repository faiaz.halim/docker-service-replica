include .env
SHELL = /bin/zsh

update:
	git pull
	cp .env_ .env

#docker swarm commands

swarm-init:
	docker swarm init --advertise-addr=$(SWARM_LEADER_DATA_HOST) --data-path-addr=$(SWARM_LEADER_DATA_HOST)
	
swarm-destroy:
	docker swarm leave -f

swarm-pull:
	docker pull $(HAPROXY_IMAGE)
	docker pull $(HELLOWORLD_IMAGE)

swarm-create:
	docker stack deploy --compose-file=<(docker-compose -f fig.dev.yml config) $(SWARMENV) --prune --with-registry-auth 

swarm-update:
	docker service update --image $(HAPROXY_IMAGE) --force $(SWARMENV)_haproxy --with-registry-auth
	docker service update --image $(HELLOWORLD_IMAGE) --force $(SWARMENV)_hello_world --with-registry-auth

swarm-clean:
	docker system prune -f 

swarm-up:
	make swarm-pull swarm-create swarm-clean

swarm-service-update:
	make swarm-pull swarm-update swarm-clean

swarm-log-haproxy:
	docker service logs -f $(SWARMENV)_haproxy

swarm-status-haproxy:
	docker service ps $(SWARMENV)_haproxy 

swarm-log-hw:
	docker service logs -f $(SWARMENV)_hello_world

swarm-status-hw:
	docker service ps $(SWARMENV)_hello_world 

swarm-remove:
	docker stack rm $(SWARMENV)

swarm-network-create:
	docker network create --opt encrypted --driver overlay --attachable $(SWARMENV)-network

swarm-network-remove:
	docker network rm $(SWARMENV)-network

swarm-node1-label:
	docker node update --label-add haproxy=true --label-add helloworld=true $(N1)
	
swarm-node2-label:
	docker node update --label-add helloworld=true $(N2)

